import React from 'react'
import {
  BrowserRouter as Router,
  Route,
} from 'react-router-dom'
// import { MemoryRouter } from 'react-router';
import Homepage  from './Views/Homepage';

const About = () => (
  <div>
    <h2>About</h2>
  </div>
)

const Routes = () => (
    <Router>
    <div>
      <Route exact path="/" component={Homepage}/>
      <Route path="/about" component={About}/>
    </div>
  </Router>
)

export default Routes